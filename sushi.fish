function sushi -d "Run a fish function as root"
    if type -q sudo
        sudo fish -c "$argv"
    else if type -q pkexec
        pkexec fish -c "$argv"
    end
end
