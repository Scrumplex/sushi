sushi - superuser shell lift
----------------
Elevate permissions to run a fish functions as root

---

sushi is a small function, that runs a fish function in an elevated (lifted) environment.

# installation
using fisher:
```sh
fisher add gitlab.com/Scrumplex/sushi
```

## requirements
sushi requires either sudo or polkit (pkexec). It prefers sudo over polkit though.


# example usage
```
# lists globally installed fisher packages
$ sushi fisher ls

# installs a fisher package globally
$ sushi fisher add gitlab.com/Scrumplex/xy

# OR (specifically for fisher)
$ susher ls
$ susher add gitlab.com/Scrumplex/xy
```

# setting up system wide fisher installation
```
# point fisher_path to correct path and save
$ sushi set -U fisher_path /etc/fish

# run initial fisher
$ susher

# install packages
$ susher add gitlab.com/Scrumplex/xy
```

# license
licensed under bla bla **GPL3**.
